#!/bin/sh
sum=$(lynx -dump $1 | grep -Eo '(http|https)://[^/"]+' | wc -l)
domain=$(echo $1 | sed 's~http[s]*:/~~g')
subs=$(echo $1 | sed 's~http[s]*://'~~g | awk '{print "/*."$0""}')
maindomain=$(lynx -dump $1 | grep -Eo '(http|https)://[^/"]+' | grep $domain | wc -l)
localdomains=$(lynx -dump $1 | grep -Eo '(http|https)://[^/"]+' | grep $subs | wc -l)
echo $maindomain
echo $[sum - localdomains]
echo $[localdomains - maindomain]
